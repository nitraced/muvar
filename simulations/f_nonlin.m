function fx = f_nonlin(thetad,theta,xd)
%F_NONLIN
%    FX = F_NONLIN(THETAD,THETA,XD)

%    This function was generated by the Symbolic Math Toolbox version 8.1.
%    30-Nov-2019 18:36:33

t2 = cos(theta);
t3 = sin(theta);
t4 = t2.^2;
t5 = t4-1.1e1;
t6 = 1.0./t5;
t7 = t2.*1.0e1;
t8 = thetad.^2;
t9 = t7-t8;
fx = [xd;t3.*t6.*t9;thetad;t3.*1.0e1-t2.*t3.*t6.*t9];
