% Script designed for the degign and the validation of our controllers
% I recomend to exectue sectionwise this script (this will follow the
% technical report
% O) Simulator inputs

% To print a modest animation set to 0 if undesired
simulator_animation = 1;

%% 1) Stabilisator Synthesis 
% Section 2
controler.type = 0; % 0 : "pure feedback with given states"
% Manual synthesis
% Nominal matrices A,B are controllable pair hence such a design is
% feasible. We decided to place those four poles :
desired_poles = [-1.5,-1.5+0.5j,-1.5-0.5j,-2];
% Test conditions :
% Disturbance on the angle command at half time and steps of 1 m and 10 deg
% to be tracked by our pendulum...
input.disturbance1 = 0;
input.disturbance2 = 0.2;
input.disturbance_time = 7.5;
input.reference1 = 1;
input.reference2 = deg2rad(10);
% Used later
input.sinus_ampl = 0;
input.sinus_freq = 0;
input.effort_max = 0;
% Time of simulation
Tsim = 15;
% Calculus of this matrix in the report
K =[
    25 30 -10 0;
    2.5 3 13 7/2
    ]
% Verification
Abf = A - B*K;
eig(Abf)
frobenius_ourgain = norm(K,'fro')
% Precommand gain calculus
H = (C*(-A+B*K)^-1*B)^-1
%% Linear Validation
model.type = 0; % Linear model
% Launch the simulation and visualize the results
launch_simulator
print_graphics(results,'jcano_feedback_lin')
%% Nonlinear validation
model.type = 1; % Nonlinear model
% Launch the simulation and visualize the results
launch_simulator
print_graphics(results,'jcano_feedback_nonlin')
%% Matlab (place) Syntesis
% Second synthesis with place
% Place poles
K = place(A,B,desired_poles);
frobenius_matlab_gain = norm(K,'fro')
% Precomputed gain calculus
H = (C*(-A+B*K)^-1*B)^-1;
%% Linear Validation
model.type = 0; % Linear model
% Launch the simulation and visualize the results
launch_simulator
print_graphics(results,'matlab_feedback_lin')
%% Nonlinear validation
model.type = 1; % Nonlinear model
% Launch the simulation and visualize the results
launch_simulator
print_graphics(results,'matlab_feedback_nonlin')

%% 2) Observer design
controler.type = 1; % 1: full state observer and feedback
% Observer matrices (perfectly estimated)
obs.A = A;
obs.B = B;
obs.C = C;
% A, C is an observable pair, hence we can design a full state observer in
% order to estimate x
% The observer has
% We have an initial estimation error of 10 degrees and 0.5 meters
input.obsxo = [0.5 0 deg2rad(10) 0]';
% We use duality between commandability and observability and we place
% poles 5 times faster than the dynamics
L = place(A',C',desired_poles*5);
% The previous resulte was the trasposition of the Luenberger gain (duality
% so we have to retranspose it)
L = L'
%% Linear Validation
model.type = 0; % Linear model
% Launch the simulation and visualize the results
launch_simulator
print_graphics(results,'observer_lin')
%% Nonlinear validation
model.type = 1; % Linear model
% Launch the simulation and visualize the results
launch_simulator
print_graphics(results,'observer_nonlin')

%% 3) Observer with augmented states
controler.type = 2; % 2: full state observer and integral feedback
% The state is augmented hence the poles cardinal also... n+2! We add two
% conjugate poles (tho aligned strucures has to be placed)
desired_poles_augmented = [-1.5,-1.5+0.5j,-1.5-0.5j,-2, -2+0.5j,-2-0.5j];
% Note that observer is unchanged (not on the same loop)
% We augment A and B matrices (for the conception only) using concatenation
Aa = [A zeros(4,2); -C zeros(2,2)];
Ba = [B; zeros(2,2)];
% Controlabity test (for verification only)
if rank(ctrb(Aa,Ba))==6
    disp('Controlabilty test passed, Mcom is full-rank')
else
    error('Contralabilty error!')
end
Ka = place(Aa,Ba,desired_poles_augmented);
% The four first coefficients rows are the state feeback
K = Ka(:,1:4);
% But the other are MINUS the integral coefficient according to our scheme
% conventions!
Ki = -Ka(:,5:6);
% Linear Validation
model.type = 0; % Linear model
% Launch the simulation and visualize the results
launch_simulator
print_graphics(results,'observer_integral_lin')

%% Nonlinear validation
model.type = 1; % Nonlinear model
% Launch the simulation and visualize the results
launch_simulator
print_graphics(results,'observer_integral_nonlin')


%% 4) Nonlinear validation with sinus disturbances and saturation 
%%% All following code is related to section 6 but not in the right order 
% 6.3 then 6.1 then 6.2 (because 6.1 and 6.2 are quite different programmly
% speaking)
%%%
% Section 6.3
Tsim = 20;
model.type = 2; % Nonlinear model with saturations and disturbances enabled
input.sinus_ampl = 2;
input.sinus_freq = 10; % Frequency in rad/s
input.effort_max = 10; % 
input.disturbance2 = 0;
input.disturbance1 = 0;
input.disturbance_time = 0;
input.reference2 = deg2rad(55);
% Launch the simulation and visualize the results
launch_simulator
print_graphics(results,'integral_worst')

%% 4') Nonlinear validation with constant dx=45% u_max disturbance and saturation
Tsim = 20;
model.type = 2; % Nonlinear model
input.sinus_ampl = 0;
input.sinus_freq = 10;
input.effort_max = 10;
input.disturbance2 = 0;
input.disturbance1 = -4.5;
input.disturbance_time = 0;
input.reference2 = deg2rad(55);
% Launch the simulation and visualize the results
launch_simulator
print_graphics(results,'integral_worst2')


%% 5) Nonlinear validity domain
simulator_animation = 0;
% Section 6.1
% We take the simple nonlinear model
model.type = 1;
Tsim = 15;
N = 25;
thetas_r = linspace(10,90,N);
errors = zeros(3,length(thetas_r));
for j=1:3
    controler.type = j-1;
    if controler.type == 2
        K = Ka(:,1:4);
        Ki = -Ka(:,5:6);
    else
        % State feedback
        K = place(A,B,desired_poles);
    end
    for i=1:N
        % No disturbances and varying references
        input.disturbance2 = 0;
        input.disturbance1 = 0;
        input.reference2 = deg2rad(thetas_r(i));
        launch_simulator
        errors(j,i) = abs(results.output.signals.values(end,2)-results.reference.signals.values(end,2));
    end
end

figure
plot(thetas_r,rad2deg(errors),'--')
xlabel("$\theta_r$ (degres)",'Interpreter','latex','FontSize',16)
ylabel("$|\theta_r-\theta(t=15s)|$ (degres)",'Interpreter','latex','FontSize',16)
legend({"State feedback","State feedback with observer","State feedback augmented with observer"},'Interpreter','latex','FontSize',16)
grid on

%% 6) Observer robustness to parametrical errors
simulator_animation = 1;
close all
% Section 6.2
% We are in the linear case with linear observer
controler.type=1; % Linear observer
model.type=0; % Linear model
% State feedback
K = place(A,B,desired_poles);

percent = 0.85; %A slight underevaluation of 15% (epsilon = -0.15)
obs.A = percent*A;
obs.B = B;
input.disturbance2 = 0;
input.disturbance1 = 0;
Tsim = 30;

launch_simulator
print_graphics(results,'parametric15percent')
% Stable but... undesired behaviour!

%% Integral test
% Naively we can state that an integral will handle this simply
controler.type=2; % Linear observer with integrator
model.type=0; % Linear model

launch_simulator
print_graphics(results,'parametric15percent_int')
% Be aware... integral has introduced augmented states and thus if we have
% a wrong A then we can reach unstabilty...

%% Function creation for eig symbolics (it is yet on the directory)
% syms epsilon real
% k = 1 + epsilon;
% lambdas = eig(A*k-L*C);
% lambdas = matlabFunction(lambdas,'File','lambdas')

%% «Eig locus» of lambdas 
% We demonstrate here the influence of the error of parametrical
% estimations for observer-based control
close all
eps = linspace(-0.5,0.5,100);
vap = lambdas(eps)';
subplot(2,1,1)
plot(eps,real(vap),'--')
grid on
legend({'$\lambda_1$','$\lambda_2$','$\lambda_3$','$\lambda_4$'},'Interpreter','latex','FontSize',14)
xlabel("$\epsilon$",'Interpreter','latex','FontSize',16)
ylabel("Real part",'Interpreter','latex','FontSize',16)
subplot(2,1,2)
plot(eps,imag(vap),'--')
grid on
hold on
xlabel("$\epsilon$",'Interpreter','latex','FontSize',16)
ylabel("Imaginary part",'Interpreter','latex','FontSize',16)