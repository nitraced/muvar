% Script done in order to run the simulation more efficiently
% Run the simulation
sim('simulator')
% Buffer the results in a same structure
results.estimation = estimation;
results.command=command;
results.reference=reference;
results.disturbance=disturbance;
results.output=output;
% Remove temp vectors
clear estimation command reference disturbance output
if simulator_animation
    animation(results)
end