function animation(results)
% Draw a basic animated scheme of our pendulum
y = results.output.signals.values;
figure(666)
% Precompute the values for the animation
xpendulum=[y(:,1),y(:,1)+3*sin(y(:,2))];
ypendulum=[0*ones(size(y(:,1))),3*cos(y(:,2))];
disp("launch animation...")
for i=1:length(results.output.signals.values)
    % Dummy pause... to force dynamic plot (lazy way...)
    pause(0.00001)
    if mod(i,10)==0
        figure(666)
        % Draw the pendulum
        plot(xpendulum(i,:),ypendulum(i,:),'Color','blue','Linewidth',3);
        hold on
        grid on
        t = results.output.time(i);
        s = strcat("Cart and angle dynamic plot t=",num2str(t));
        title(s,'Interpreter','latex','Fontsize',16)
        xlabel('x axis')
        %Draw the cart
        rectangle('Position',[xpendulum(i,1)-0.25,-0.25,0.5,0.5],'FaceColor','b')
        hold off
        % Ratio aspects
        xlim([-10 10])
        ylim([-1,3])
        pbaspect([1 0.2 1])
    end
end
disp("Animation ended!")
end