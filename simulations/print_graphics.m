function print_graphics(results,testname)
% Print all interesting graphics of your simulation automatically. Lazyness
% from my part. testname is the name of the folder that you want to create  
% First our test has its own folder
close all
if nargin==2
mkdir(testname)
else
 testname="";   
end
%% Estimator error
figure(1)
plot(results.estimation.time,results.estimation.signals.values(:,1:2))
ylabel('States [m,m/s]','Interpreter','latex','FontSize',14)
grid on
hold on
yyaxis right
plot(results.estimation.time,results.estimation.signals.values(:,3:4))
legend({'$x-\hat{x}$','$\dot{x}-\dot{\hat{x}}$','$\theta-\hat{\theta}$','$\dot{\theta}-\dot{\hat{\theta}}$'},'Interpreter','latex','FontSize',14,'Location','best')
xlabel('Time [s]','Interpreter','latex','FontSize',14)
ylabel('States [rad,rad/s]','Interpreter','latex','FontSize',14)

if nargin==2
figure(1)
s = strcat(testname,"/","est.eps");
print(s,'-depsc')
end
%% Controler performance

figure(2)
% On x state first
subplot(1,2,1)
plot(results.output.time,results.output.signals.values(:,1))
grid on
hold on
plot(results.reference.time,results.reference.signals.values(:,1),'--')
plot(results.disturbance.time,results.disturbance.signals.values(:,1),'.')
legend({'$x$','$r_x$','$d_x$'},'Interpreter','latex','FontSize',14,'Location','best')
xlabel('Time [s]','Interpreter','latex','FontSize',14)
ylabel('Abscisse [m], disturbance [N]','Interpreter','latex','FontSize',14)

% On theta secondly
subplot(1,2,2)
plot(results.output.time,results.output.signals.values(:,2))
grid on
hold on
plot(results.reference.time,results.reference.signals.values(:,2),'--')
% In this case... the sinus was jamming our curves so I removed it
if testname ~= "integral_worst"
plot(results.disturbance.time,results.disturbance.signals.values(:,2),'.')
legend({'$\theta$','$r_\theta$','$d_\theta$'},'Interpreter','latex','FontSize',14,'Location','best')
else
legend({'$\theta$','$r_\theta$'},'Interpreter','latex','FontSize',14,'Location','best')
end
xlabel('Time [s]','Interpreter','latex','FontSize',14)
ylabel('Angle [rad], disturbance [N]','Interpreter','latex','FontSize',14)
set(gcf, 'Position',  [100, 100, 1000, 500])

if nargin==2
figure(2)
s = strcat(testname,"/","ctr.eps");
print(s,'-depsc')
end

%% Command effort 
figure(3)
plot(results.command.time,results.command.signals.values)
grid on
legend({'$u_1$','$u_2$'},'Interpreter','latex','FontSize',14,'Location','best')
xlabel('Time [s]','Interpreter','latex','FontSize',14)
ylabel('Commands [N]','Interpreter','latex','FontSize',14)

if nargin==2
figure(3)
s = strcat(testname,"/","com.eps");
print(s,'-depsc')
end

end

