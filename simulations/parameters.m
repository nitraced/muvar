clear all
close all
clc
%% Parameters of the model
% In order to generate the dynamic model and associated functions
dyna.model_generation =0;
% Dynamic constants
m = 1;
M = 10;
g = 10;
l = 1;

%% Dynamics generator
if dyna.model_generation == 1;
    % Symbolic model generation
    syms theta thetad u1 u2 x xd real
    % State vector
    X = [x xd theta thetad]';
    % Command vector
    u = [u1 u2]';
    % f(x) nonlinear function
    alpha = (m+M-m*cos(theta)^2)^-1;
    fx = [xd;
        alpha*m*sin(theta)*(-g*cos(theta)+l*thetad^2);
        thetad;
        sin(theta)*g/l-(alpha*cos(theta)*sin(theta)*m/l)*(-g*cos(theta)+l*thetad^2)]
    % g(u) nonlinear function
    gu = [0;
        alpha*u1;
        0;
        u2/(m*l)-alpha*cos(theta)*u1/l]
    % Dfx tangent linearized of f (callable at the equilibrium)
    Df = jacobian(fx,X)
    % Dgu tangent linearized of g
    Dg = jacobian(gu,u)
    
    % Function creation
    matlabFunction(fx,'File','f_nonlin');
    matlabFunction(gu,'File','g_nonlin');
    matlabFunction(Df,'File','Df_X');
    matlabFunction(Dg,'File','Dg_u');
    
end

%% Creation of the linearized model at the equilibrium point
% We use previous generated functions
% Initial condition system at rest at unstable equilibrium point
X0 = [0 0 0 0]';
% Equilibrium state X* = [x*,0,0,0] (with null accelerations)
% DF = Df_X(THETAD,THETA)
% Equilibrium : null derivatives
% Unstable theta = 0
A = Df_X(0,0);
% B is only theta dependent
%  DG = Dg_u(THETA)
% Then
B = Dg_u(0);
% The relation is linear and D matrix is null (unsimuled then)
% (no direct relation between u and y)
C = [1 0 0 0;
    0 0 1 0];
%% Basic analysis
% The pair A,B is it controlable?
Mcom = ctrb(A,B)
rank(Mcom)
% Yes
% The pair AC is it observable?
Mobs = obsv(A,C)
rank(Mobs)
% Yes




