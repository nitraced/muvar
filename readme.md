# Projet de cours ELE6202

Par Justin Cano, Décembre 2019.

<u>Asservissement d'un pendule inversé.</u>

## Contenu

**Dossier simulateur :** contient les codes Matlab ayant été utiles pour la modélisation, l'analyse et la conception du système de commande inhérent au système.

**Dossier rapport :** contient les sources .tex et le rapport PDF du projet.

## Intructions d'exécution

D'abord exécuter le script parameters pour générer les paramètres de simulation.

Ensuite, exécuter *controler_synthesis* étape par étape pour voir les différentes synthèses. Si la visualisation du chariot n'est pas souhaitée, mettre le flag au début du script à 0.

En ce qui concerne l'architecture du code, brièvement nous avons :

- Le script *parameters* permet de créer les paramètres de simulation et les fonctions du modèle non linéaire et non-linéarisé.
- Le scritpt *controler_synthsis* permet de synthétiser et de valider tous les contrôleurs décrits dans le rapport. Il appelle :
  - *simulator.slx* un script simulink fait sous la version R2018.b (décrit plus bas) appelé à chaque validation avec différent paramètres.
  - *launch_simulator* aide l'appel précédent, peut lancer une annimation graphique de mon cru, à la discretion du lecteur. Elle produit une structure de données *results* qui rassemble **toutes les données utilies produites par Simulink**.
  - *print_graphics* fonction à deux variables, permettant la visualisation des graphiques en lui donnant une structure en première entrée. La deuxième variable est une chaîne de caractère si vous souhaitez avoir les figures en format .eps dans un répertoire du nom de ladite chaîne...

## Simulateur Simulink

Je l'ai réalisé avec des blocs conditionnels en fonction du contrôleur utilisé ou du modèle de validation fourni ces derniers varient. Cependant toute l'exécution et l'output des résultats est automatisée dans le script de synthèse dont je parle à la section précédente. 

Des appels aux fonctions des modèles non-linéaires présentes dans le répertoires se font également pour plus de lisibilité. Ce sont les fonctions consignées dans le rapport mais générées symboliquement, permettant une vérification de notre démarche et une minimisation des erreurs dites "d'interface cerveau-clavier" (ou d'étudiant étourdi, cela est équivalent).

Bonne exécution du code. 

## Droits de réutilisation et contact

Ce code est réutilisable sous licence MIT libre. Je peux donner accès au GIT à la demande.

Pour toute question : justin.cano@polymtl.ca

Document édité sous Typora, un éditeur de *markdown* que je recommande vivement.