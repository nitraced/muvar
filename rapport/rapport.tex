\documentclass[11pt,letterpaper,twocolumn]{article}
%\documentclass[letterpaper, 11 pt, conference]{ieeeconf}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{graphicx}
\usepackage{lmodern}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{todonotes}
\usepackage{amsthm}
\author{\large{\textsc{Justin Cano}}}
\title{Asservissement d'un pendule inversé.}
\date{Mini-projet \emph{ELE6202} -- Session d'automne 2019}
\usepackage[colorlinks]{hyperref}
\hypersetup{
 %colorlinks=false,
 linkcolor={red},
 citecolor={green},
 %allcolors={MidnightBlue}
}

\graphicspath{{../simulations/}}


% Commandes spéciales

\renewcommand{\th}{\theta}
\newcommand{\dth}{\dot \th}
\newcommand{\ddth}{\ddot \th}
\newcommand{\mbf}{\mathbf}
\newcommand{\dx}{\dot x}
\newcommand{\ddx}{\ddot x}
\newcommand{\bx}{\mbf x}
\newcommand{\bhx}{\hat \bx}
\newcommand{\by}{\mbf y}
\newcommand{\bu}{\mbf u}
\newcommand{\bhy}{\hat \by}
\newcommand{\be}{\mbf e}
\newcommand{\bdx}{\dot \bx}

\renewcommand{\sin}{\mathrm{S}}
\renewcommand{\cos}{\mathrm{C}}

\newcommand{\td}{\todo[inline]}

\theoremstyle{prop}
\newtheorem{prop}{Proposition}[section]
\theoremstyle{remark}
\newtheorem*{remark}{Remarque}
 


\begin{document}
\maketitle
\section{Introduction}
Notre étude porte sur la commande multivariée d'un pendule inversée, système naturellement instable. Nous présentons tout d'abord la modélisation du système à la section \ref{ss:modelisation}. Ensuite nous synthétisons puis validons successivement une commande stabilisante à retour d'état (section \ref{ss:stabilisatrice}), un observateur d'état complet (section \ref{ss:observateur}) ainsi qu'une commande augmentée par action intégrale (section \ref{ss:augmented}). Nous achevons notre discussion à la section \ref{ss:robustesse} sur implémentation de tels contrôleurs en pratique et leurs limitations.

\section{Modélisation du problème}
\label{ss:modelisation}

\subsection{Obtention des équations d'état}
 La mécanique lagrangienne nous donne deux équations couplées pour le système:
\begin{equation}
\begin{cases}
(m+M)\ddx + ml(\dth \cos\th - \dth^2 \sin \th) = u_1 + u_2 \cos \th \\
m(\ddx \cos\th + l\ddth - g\sin \th) = u_2 .
\end{cases}
\label{eq:couplees}
\end{equation}
 Nous notons les les fonctions sinus S et cosinus C dans un souci de présentation pour le reste du document. Afin d'obtenir une équation d'état du type $\bdx = f(\bx) + g(\bu)$ en posant le vecteur d'état $\bx = \begin{bmatrix}
 x & \dx & \th & \dth
\end{bmatrix}^\top$ 
il nous faut manipuler le système (\ref{eq:couplees}). La deuxième équation nous donne ($m,l\neq 0$) :
\begin{equation}
\ddth = u_2/ml + \sin(\theta) g/l - \cos \theta \ddx/l,
\label{eq:thetaddasubstituer}
\end{equation} 
ainsi les deuxièmes composantes de $f$ et $g$ se trouvent aisément en la substituant dans la première équation de (\ref{eq:couplees}) et donc :
\begin{equation}
f_2(\bx)+ g_2(\bu) = \ddx = \alpha (- mg\sin \th \cos \theta + ml \dth^2 \sin \theta + u_1 ) 
\label{eq:f2}
\end{equation}
avec $\alpha := (M+m-m\cos^2 \theta)^{-1}$ défini $\forall x \in \mathbb{R}^4$ car $M+m>m$. Nous pouvons ensuite utiliser (\ref{eq:f2}) pour rendre (\ref{eq:thetaddasubstituer}) dépendante uniquement des variables contenues dans $\bx$ et nous donner les composantes $f_4$ et $g_4$. Au final le modèle est le suivant :

\begin{align}
\bdx &= f(\bx) + g(\bu) \text{ avec :} \nonumber \\
f(\bx) &= 
\begin{bmatrix}
\dx \\
\alpha m \sin \theta(-g  \cos \theta + l\dth^2 ) \\
\dth \\
\sin \th g/l - \alpha \cos \th  m \sin \theta(-g  \cos \theta + l\dth^2 ) / l 
\end{bmatrix} \nonumber \\ 
g(\bu) &= 
\begin{bmatrix}
0 \\
\alpha u_1 \\
0 \\
u_2/ml -  \alpha \cos \theta u_1  /l  
\end{bmatrix}
\label{eq:dyna}
\end{align}


Remarquons de plus que les sorties du système sont $\th$ et $x$ donc logiquement, l'équation de sortie est linéaire et indépendante de la commande $\mbf D =\mbf 0_{2 \times 2}$ :
\[
\by = \mbf C \bx, \text{ avec } \mbf C = 
\begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 0 & 1 & 0
\end{bmatrix}.
\]

\subsection{Linéarisation du modèle nominal}
\label{ss:linenominal}
Nous nous intéressons au contrôle du pendule inverse autour de sa position d'équilibre instable $\bx^*$ (verticale $\theta^* = 0$), à l'équilibre $\dx^* = \dth^* = 0$ et $x^*$ est une coordonnée quelconque du chariot (qui n'apparaîtra pas dans les jacobiennes des fonctions de (\ref{eq:dyna}) qui n'ont que pour variables explicites du couple $\{\dth,\th\}$). Après calcul on obtient les matrices $\mbf A$ et $\mbf B$ suivantes :
\begin{align*}
\mbf A &= \left. \frac{\partial f}{\partial \bx} \right\vert_{\bx = \bx^* , \bu = \bu^*}
=
\begin{bmatrix}
     0  &   1  &   0  &   0 \\
     0  &   0  &  -1  &   0 \\
     0  &   0  &   0  &   1 \\
     0  &   0  &  11  &   0
\end{bmatrix} \\
\mbf B &=  \left. \frac{\partial g}{\partial \bu} \right\vert_{\bx = \bx^*,\bu = \bu^*}
= 
\begin{bmatrix}
    0   & 0 \\
    0.1 & 0 \\
    0   & 0 \\
   -0.1 & 1 
\end{bmatrix}
\end{align*}

\section{Commande stabilisatrice}
\label{ss:stabilisatrice}

\subsection{Motivation et faisabilité}
Le système linéarisé dans la section \ref{ss:linenominal} présente des \textbf{pôles instables} \footnote{Logique au vu du point d'équilibre choisi.}. En effet, $\mathrm{tr}(\mbf A) = 0$ prouve que les pôles ne sont pas tous à partie réelle strictement négative, ce qui motive notre conception ici. Nous nous devons de créer un compensateur stabilisateur dudit système.


\begin{prop}
Le système est \textbf{complètement commandable}. 
\label{prop:cdb}
\end{prop}
\begin{proof} (Par le critère de Kalman)
On calcule la matrice de comandabilité du système :
\[ 
\small
\begin{bmatrix}
\mbf B  \vdots \mbf A \mbf B \vdots \mbf A^2 \mbf B \vdots \mbf A^3 \mbf B 
\end{bmatrix}
=
\left[
\begin{matrix}
0     &   0  &  0.1   &  0  \\     
0.1   &    0  &  0    &  0  \\
0     &    0  & -0.1  &  1  \\      
-0.1  &    1  &  0    &  0
\end{matrix}
\vdots
\mbf A^2 \mbf B 
\vdots
\mbf A^3 \mbf B  
\right]
\normalsize
\]
les quatre premières colonnes de cette matrices sont clairement indépendantes, leur déterminant valant $\frac{-1}{100}$. Ceci prouve que le rang de la matrice de contrôlabilité est de 4, donc elle est de rang plein. 
\end{proof}
Nous pouvons donc concevoir une commande plaçant les pôles en tout endroit du plan $\mathbb{C}$, en particulier dans $\mathbb{C}^{-} = \{s\in \mathbb{C}, \mathrm{Re}(s)<0.\}$.
\subsection{Retour d'état}
\label{ss:stabilisateur}

Nous appliquerons une loi de commande par retour d'état. Nous nous donnerons tout l'état $\mbf x$, synthétisons un gain $\mbf K$ tel que $\mbf u = -\mbf K \mbf x$ afin de placer les pôles en :
\[
p_\textsc{des}= \left\{-\frac{3}{2}\pm \frac{1}{2}j, -\frac{3}{2}, -2\right\},
\]
nous remarquons tout de suite que ces pôles sont admissibles (conjugués complexes où réels), au nombre de quatre (dimension de l'état et le système est complètement commandable) et stables. Notre stratégie est de les placer en approche dite alignée pour les trois premiers pôles (deux conjugués encadrant un réel en $-3/2$) et avec un fort amortissement pour les conjugués $\xi = \text{cos}(\text{arg}(p_i))$, ici on a $\xi\approx 0.9$ ce qui donne un très faible dépassement en réponse. Le dernier pôle est situé en $-2$.
Nous avons choisi ces pôles, relativement lents, afin d'obtenir des commandes $u_i$ de valeurs raisonnables. 

Notons que note étude ne porte pas sur le choix de placement des pôles mais plutôt sur le moyen d'y parvenir.

Pour un gain générique $\mbf K \in \mathbb{R}^{2\times4} = [k_{ij}]$, nous obtenons la matrice en boucle fermée $\mbf A^c = \mbf A - \mbf B \mbf K$ :
\begin{equation}
\small
\mbf A^c = \left[\begin{array}{cccc} 0 & 1 & 0 & 0\\ -\frac{k_{11}}{10} & -\frac{k_{12}}{10} & -\frac{k_{13}}{10}-1 & -\frac{k_{14}}{10}\\ 0 & 0 & 0 & 1\\ \frac{k_{11}}{10}-k_{21} & \frac{k_{12}}{10}-k_{22} & \frac{k_{13}}{10}-k_{23}+11 & \frac{k_{14}}{10}-k_{24} \end{array}\right],
\label{eq:boucleK}
\normalsize
\end{equation} 
nous décidons d'imposer les gains pour que $\mbf A^c$ soit diagonale par blocs :
\[
\mbf A^c = \text{diag}(\mbf A^c_{11},\mbf A^c_{22}), \text{ avec } \mbf A^c_{ii} \in \mathcal{M}_2(\mathbf{R})
\]
pour le placement des pôles car $\sigma(\mbf A^c) = \sigma(\mbf A^c_{11})\cup \sigma(\mbf A^c_{22})$ dans ces conditions. Pour ceci, imposons :
\[
k_{21}=k_{11}/10,~k_{22} = k_{12}/10,~k_{13} = -10,~k_{14} =0.
\]
Nous avons donc :
\[ 
\small
\mbf A^c_{11}
= \frac{1}{10}\begin{bmatrix}
0 & 10 \\
-k_{11} & -k_{12}
\end{bmatrix},
~
\mbf A^c_{22}
= \begin{bmatrix}
0 & 1 \\
-k_{23}+10 & -k_{24}
\end{bmatrix}
\normalsize
\]
nous allons assigner les pôles $\lambda_{1,2}=-\frac{3}{2}\pm \frac{1}{2}j$ à $\mbf A^c_{11}$ nous imposons donc $\mathrm{Tr}(\mbf A^c_{11}) = \lambda_1 + \lambda_2 = -3$ et $|\mbf A^c_{11}|= \lambda_1 \lambda_2 = 5/2$. Donc $k_{12} = 30$ et  $k_{11}/10 = 5/2 \Rightarrow k_{11}=-25$. Donc $k_{21}=5/2$ et $k_{22} = 3$.


Nous assignerons les deux pôles restants $\lambda_{3}=-\frac{3}{2}$ et $\lambda_4 = -2$ à $\mbf A^c_{22}$. D'une manière analogue $\mathrm{Tr}(\mbf A^c_{22}) = \lambda_3 + \lambda_4 = -k_{24} = -7/2$ donc $k_{24}=7/2$. Pour le dernier gain $|\mbf A^c_{22}|= \lambda_3 \lambda_4 = 3 = k_{23}-10$ donc $k_{23}=13$.
Ce qui donne le gain suivant :
\[
\mbf K = 
\begin{bmatrix}
 25 & 30 & -10 &  0 \\
 5/2 & 3 & 13 & 7/2
\end{bmatrix}
, 
\text{ et }
||\mbf K||_F = 42.7
\]
\begin{remark}
Ce gain n'est pas unique, en guise d'exemple, on aurait pu inverser les rôles des $\mbf A_{ii}^c$ dans le placement des pôles. Également, on aurait pu rendre la matrice compagne par blocs.
\end{remark}
Le gain trouvé n'est pas optimal au sens de la norme de Frobenius, ce qui peut mener à des commandes plus importantes. Le gain trouvé dans Matlab par la fonction \texttt{place()} est $\tilde{\mbf K}$ et nous constatons que $||\mbf K||_F < ||\tilde{\mbf K}||_F = 49.8$ nous allons comparer les commandes engendrées par ces deux gains à la fin de la présente section.  
 
\subsection{Gain de précommande}
La dynamique modifiée n'engage en rien que la poursuite d'un signal soit satisfaisante en régime permanent. Afin de régler ce problème, il nous faut pondérer la référence par un gain en entrée du contrôleur que nous nommerons \textbf{gain de précommande}. Notons que ce dernier est fonction du gain $\mbf K$ précédemment trouvé.
\begin{prop}
Le gain de précommande $\mbf H$ pour le bouclage réalisé en section \ref{ss:stabilisateur} est donné par 
\[\mbf H = [\mbf C (-\mbf A + \mbf{BK})^{-1} \mbf B]^{-1}.\]
\label{prop:prec}
\end{prop}
\begin{proof}
La matrice de transfert $\mbf G(s)$ du système en boucle fermée est donnée par :
\[
\mbf r(s) = \mbf C (s \mbf I-\mbf A + \mbf{BK})^{-1} \mbf B \mbf y(s) := \mbf G(s) \mbf y(s)
\]
en appliquant le théorème de la valeur finale pour un échelon $\mbf r(t)=[\alpha~\beta]^\top u_{-1}(t)$ $\alpha,\beta \in \mathbb{R}^2$, sous réserve de stabilité (la proposition \ref{prop:cdb} permettant de stabiliser le système), nous obtenons :
\[ \mbf y_\infty = \mbf G(0) \mbf r(\infty) := \mbf H^{-1} \mbf r(\infty). \]
Remarquons que $\mbf G(0) \in \mathcal{M}_2(\mathbb{R})$ existe car $\text{Re}(\lambda(-\mbf A + \mbf{BK})) > 0$ et est de plein rang (inversible).
\end{proof}

\subsection{Validation sur le modèle linéaire}
\label{ss:valid-re}
\begin{figure}
\includegraphics[width=\columnwidth]{fig/precommande.pdf} 
\caption{Architecture du contrôleur dans l'espace d'état avec gain de précommande.}
\label{sch:precommande}
\end{figure}



Nous implémentons le contrôleur à gain de précommande de la figure \ref{sch:precommande} sur Matlab. Notre cas de test est le suivant, nous essayons de poursuivre une référence de $\mbf \theta_x = 1\mathrm{m}$ et $y_\theta = 10 \deg$. Nous nous donnons l'état au complet pour effectuer notre retour. Nous rajoutons à mi-temps de la simulation ($7.5s$) une perturbation (entrée non-contrôlée sur $\bu$) au bout pendule de $d_\theta = 0.2~N$ afin de tester la robustesse du contrôleur. 

\begin{figure}[h]
\includegraphics[width=\columnwidth]{fig/jcano_feedback_lin/ctr.eps} 
­\caption{Sorties contrôlées par retour d'état.}
\label{fig:valid_jc_lin}
\end{figure}
Comme on l'a démontré dans la proposition \ref{prop:prec}, en l'absence de perturbations, nous nous attendons à un suivi parfait de trajectoire en régime permanent. Ce qui est visible sur la figure \ref{fig:valid_jc_lin} : en l'absence de perturbation, le signal converge vers les valeurs imposées en entrée. Au niveau de la dynamique transitoire, nous avons réussi notre pari d'amortir le système suffisamment car nous ne remarquons pas de dépassement significatif. 
\begin{figure}[h]
\centering
\includegraphics[width=0.7\columnwidth]{fig/jcano_feedback_lin/com.eps} 
­\caption{Commandes avec notre gain $\mbf K$.}
\label{fig:com_jc_lin}
\end{figure}
En ce qui concerne le rejet de perturbation, comme on pouvait s'y attendre, ce contrôleur se révèle inadapté car il suppose que le modèle est idéal (donc non-perturbé) et donc on remarque une stabilisation mais pas de rejet de la perturbation. 
\begin{figure}[h]
\centering
\includegraphics[width=0.7\columnwidth]{fig/matlab_feedback_lin/com.eps} 
­\caption{Commandes avec le gain de Matlab $\tilde{\mbf K}$.}
\label{fig:com_matlab_lin}
\end{figure}

En ce qui concerne la commande $\mbf u$ calculée par le contrôleur, suivant la discussion sur les gains clôturant la section \ref{ss:stabilisateur}, on a en effet une augmentation sensible de la commande lorsque nous utilisons le gain $\tilde{\mbf K}$ (fig. \ref{fig:com_matlab_lin}) en lieu et place de $\mbf K$ (fig. \ref{fig:com_jc_lin}), nous pouvons constater une amplitude de commande plus important au niveau de $u_1(0)$. Ceci est en accord avec le fait que $||\tilde{\mbf K}||_F$ est légèrement supérieur à $||\mbf K||_F$.

La perturbation (qui est modérée) engendre naturellement une faible variation de la commande générée par le contrôleur. À la fin du test, la constance de la commande (sortie du contrôleur) prouve que le contrôleur est incapable de rejeter une perturbation dont les effets sont pourtant visibles (cf. fig. \ref{fig:valid_jc_lin}).

\begin{remark}
Nous avons fait la validation sur le modèle non-linéaire mais par souci de concision nous la présentons que dans les deux autres synthèses.
\end{remark}

\section{Observateur d'état}
\label{ss:observateur}
\subsection{Motivation et faisabilité}
En pratique, suivant la structure de $\mbf C$, il nous est possible d'observer que deux états.  Afin de mettre en œuvre d'un contrôleur à retour d'état, notre idée est de concevoir un observateur complet de l'état $\mbf x$ en se donnant des mesures $\mbf y$. Tout d'abord, étudions la possibilité de ceci. 
\begin{prop}
Le système linéarisé est \textbf{complétement observable}. 
\label{prop:obs}
\end{prop}
\begin{proof}(Par le critère de Kalman)
D'une manière analogue à la démonstration de la proposition \ref{prop:cdb}, on construit la matrice d'observabilité du système :
\[
\begin{bmatrix}
\mbf C \\
\mbf{CA} \\
\mbf{CA}^2 \\
\mbf{CA}^3
\end{bmatrix}
=
\left[
\begin{array}{c}
\begin{matrix}
1 & 0 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 1 & 0 & 0 \\
1 & 0 & 0 & 1 \\
\end{matrix}\\
\mbf{CA}^2 \\
\mbf{CA}^3
\end{array}
\right]
.
\]
la matrice précédente est clairement de rang plein, car ses quatre première lignes forment une base de $\mathbb{R}^4$ (elles sont indépendantes).
\end{proof}
Cette propriété nous permet de concevoir un observateur d'état dont la valeur estimée $\bhx$ convergera vers l'état réel $\bx$.
\subsection{Conception d'un observateur}

Un observateur de Luenberger $(\Omega)$ peut être vu comme un clone de la dynamique du système $(\Sigma) = \{\mbf A, \mbf B, \mbf C\}$ comme on peut le constater sur la figure \ref{sch:luen}.
\begin{figure}[h]
\includegraphics[width=\columnwidth]{fig/luenberger.pdf} 
\caption{Architecture du contrôleur à retour d'état + observateur.}
\label{sch:luen}
\end{figure}
Dans cette section, on supposera que les matrices de la dynamique de l'observateur implémentées comme exactement estimées $\{\mbf A',\mbf B',\mbf C'\}=\{\mbf A,\mbf B,\mbf C\}$.
La dynamique du système total incluant l'observateur (si prop. \ref{prop:obs} est vraie) est donnée par :
\begin{equation}
\begin{cases}
\dot{\mbf z} 
& :=
\begin{bmatrix}
\bdx \\
\dot{\pmb \epsilon}_x
\end{bmatrix}
=
\begin{bmatrix}
\mbf A - \mbf{BK} & \mbf{BK} \\
\mbf 0_{4\times 4} & \mbf A - \mbf L \mbf C
\end{bmatrix}
\begin{bmatrix}
\bx \\
{\pmb \epsilon}_x
\end{bmatrix}
+
\begin{bmatrix}
\mbf B \\
\mbf 0_{4\times1}
\end{bmatrix}
\mbf r
\\
\by &= \begin{bmatrix}
\mbf C & \mbf 0_{4\times 1}
\end{bmatrix}
\begin{bmatrix}
\bx \\
{\pmb \epsilon}_x
\end{bmatrix}
\end{cases}
\label{eq:state:observerstateeq}
\end{equation}
en notant $\pmb \epsilon_x = \hat{\mbf x} - \mbf x$, l'erreur d'estimation.
\begin{prop}
Les pôles de l'observateur peuvent être placés arbitrairement.
\label{prop:placementobs}
\end{prop}
\begin{proof}
 $\mbf A, \mbf C$ observable $\Rightarrow \mbf A^\top, \mbf C^T$ commandable. Donc $\exists \tilde{\mbf K}, \sigma(\mbf A^\top - \mbf C^\top \tilde{\mbf K}) = p_{des}$ or, les valeurs propres \textbf{admissibles} sont invariantes par transposition (pour les paires complexes conjuguées, l'hermitien ne fait que permuter les v.p.) $p_{des} = \sigma([\mbf A^\top - \mbf C^\top \tilde{\mbf K}]^\top) = \sigma(\mbf A - \tilde{\mbf K}^\top \mbf C])$ et $\tilde{\mbf K}^\top:=\mbf L.$
\end{proof}
\begin{prop}
 La dynamique du système au total au total est l'union de celles du système bouclé (fig. \ref{sch:precommande}) et de l'erreur d'estimation.
\label{prop:union}
\end{prop}
\begin{proof}
La matrice de dynamique dans (\ref{eq:state:observerstateeq}) est triangulaire supérieure par blocs, ceci prouvant le résultat.
\end{proof}
La proposition \ref{prop:placementobs} démontre le fait que nous pouvons choisir toute dynamique admissible. Vient s'ajouter la proposition \ref{prop:union} et le fait que nous voulons assurer une convergence rapide des estimées, autrement dit : $\pmb \epsilon_x \rightarrow 0$ rapidement. Nous avons donc intérêt à choisir des pôles d'observateur plus rapides que ceux de la dynamique toute proportions gardées afin de ne pas forcer les gains à être élevés. 
Nous prendrons des pôles valant à partie réelle cinq fois plus élevée que les pôles du système. Afin de simplifier, nous effectuerons le placement à $p_\textsc{des,OBS} = 5p_\textsc{des}$, la résolution du problème nous donne le gain :
\[
\mbf L =
\begin{bmatrix}
 17.2 &  74.1  & -3.7 & -25.8 \\
 0.2 &  2.5 &  15.3 &  73.1
\end{bmatrix}^\top.
\]
\subsection{Validation linéaire}
\label{ss:obsval}

Nous effectuons un test dans les mêmes conditions qu'à la section \ref{ss:valid-re}
\begin{figure}[h]
\centering
\includegraphics[width=0.8\columnwidth]{fig/observer_lin/ctr.eps} 
­\caption{Suivi/rejet avec observateur.}
\label{fig:obssuivi:lin}
\end{figure}
\begin{figure}[h]
\centering
\includegraphics[width=0.6\columnwidth]{fig/observer_lin/est.eps} 
­\caption{Erreurs d'estimation.}
\label{fig:obserr:lin}
\end{figure}
en ajoutant de surcroît une erreur d'estimation sur l'état initial. Notre estimé initial présente des erreurs en $x$ et $\theta$, $\bhx_1(0) = 0.5 \mathrm{m}$ et  $\bhx_3(0) = 10 \pi/180~\mathrm{rad}$. Tout de suite, nous remarquons que l'erreur d'estimation semble converger de manière rapide (fig. \ref{fig:obserr:lin}) en suivi, ceci occasionne (notre contrôleur étant muni du même gain de précommande $\mbf H$) une poursuite parfaite du signal une fois ladite erreur correctement absorbée. Les propositions \ref{prop:placementobs} et \ref{prop:union} étant illustrées en fig. \ref{fig:com_obs} (les pôles rapides de l'observateur engendrent de fortes commandes) et en fig. \ref{fig:obssuivi:lin} (système moins amorti par rapport à la fig. \ref{fig:valid_jc_lin}).
\begin{figure}[h]
\centering
\includegraphics[width=0.6\columnwidth]{fig/observer_lin/com.eps} 
­\caption{Commandes avec observateur.}
\label{fig:com_obs}
\end{figure}

Toutefois, notre synthèse n'étant pas robuste aux perturbations, non intégrées dans nos hypothèses du schéma \ref{sch:luen} comme pour le retour d'état. Pis, l'observateur n'intègre par ces écarts et présente des erreurs d'estimation des variables lorsque le système est perturbé ce qui éloigne encore plus le signal de la référence. 

\subsection{Validation non-linéaire}
Procédons au même test sur le modèle non-linéaire de (\ref{eq:dyna}) 
\begin{figure}[h]
\centering
\includegraphics[width=0.8\columnwidth]{fig/observer_nonlin/ctr.eps} 
­\caption{Suivi/rejet avec observateur.}
\label{fig:obssuivi:nonlin}
\end{figure}
logiquement, nous remarquons sur la fig. \ref{fig:obssuivi:nonlin} une comportement analogue en $x(t)$ que pour le modèle linéaire (la jacobienne n'est pas fonction explicite de $x$). Pour $\theta(t)$ on remarque des changements majeurs dû au fait que nous nous sommes éloignés du point de fonctionnement $\mbf x^* =\mbf 0$. Conséquence, la poursuite de $\theta_r$ devient faussée et le rejet de $d_r$ est détérioré.   
\begin{figure}[h]
\centering
\includegraphics[width=0.6\columnwidth]{fig/observer_nonlin/est.eps} 
­\caption{Erreurs d'estimation.}
\label{fig:obserr:nonlin}
\end{figure}
Pour ce qui est des erreurs d'estimation de la fig. \ref{fig:obserr:nonlin}, le modèle ne prend pas en compte les non-linéarités, en résultent des erreurs d'observation (la dynamique de l'erreur d'estimation perd sa stabilité asymptotique). 

\section{Retour d'état augmenté}
\label{ss:augmented}
\begin{figure}[h]
\includegraphics[width=\columnwidth]{fig/integrale.pdf} 
\caption{Retour d'état augmenté par une action intégrale.}
\label{sch:integr}
\end{figure}
Afin de permettre le rejet et la poursuite de signaux constants, nous nous devons de regarder l'écart entre la sortie et l'entrée. Nous plaçons une action intégrale comme le montre le schéma simplifié \ref{sch:integr} (sans l'observateur d'état). Ainsi, notre loi de rétroaction est la suivante :
\begin{equation}
\mbf u = \mbf K_i \mbf e_y - \mbf K \bhx, \mbf e_y = \int (\mbf y - \mbf r) dt,
\label{eq:commande:integrale}
\end{equation}
on fera l'hypothèse d'une convergence rapide $\bhx \rightarrow \bx$ pour raisonner sur $\mbf x$ en terme de dynamique.
Grâce à (\ref{eq:commande:integrale}) on peut déterminer la dynamique augmentée du système 
prenant $\mbf x_a = [\bx~\mbf e_y]^\top$ :

\begin{align}
&
\begin{cases}
\dot{\mbf x}_a = \mbf A_a \mbf x_a + \mbf B_a \mbf u + \mbf B_r \mbf r \\
\mbf y = \mbf C_a \mbf x_a
\end{cases}
\mbf A_a = 
\begin{bmatrix}
\mbf A &  \mbf 0_{4\times 2} \\
- \mbf C & \mbf 0_{2\times 2} 
\end{bmatrix}  \nonumber\\
\mbf B_a &= 
\begin{bmatrix}
\mbf B \\
 \mbf 0_{2\times 2} 
\end{bmatrix}, 
\mbf B_r = 
\begin{bmatrix}
 \mbf 0_{2\times 2}  \\
\mbf I_2
\end{bmatrix}
\mbf C_a = 
\begin{bmatrix}
\mbf C & \mbf 0_{2\times 2}
\end{bmatrix}
\label{eq:state_augmented}
\end{align}

Nous choisissons de placer les pôles $p_\textsc{des} \cup \{-2 ­\pm 0.5\}$ qui demeurent à bonne distance de ceux de l'observateur ($-2 > -\frac{15}{2}$). Ceci est faisable puisque $\mbf A_a, \mbf B_a$ est commandable (calcul effectué dans Matlab). Nous disposons, après résolution du problème de placement, donc d'un gain $\mbf K_a$, donné par \texttt{place()} dans Matlab, qui sera partitionné (notez le signe moins) ainsi en accord avec (\ref{eq:commande:integrale}) :
\[ \mbf K_a = \begin{bmatrix}
\mbf K & -\mbf K_i
\end{bmatrix}.
\]

\subsection{Validation linéaire}

Nous effectuons un test dans les mêmes conditions qu'à la section \ref{ss:obsval}. Comme nous pouvons nous y attendre les valeurs à l'échelon sont parfaitement poursuivies et rejetées à cause du fait que nous avons augmenté le type de la fonction de transfert. En effet, un échelon étant de type 1, si le contrôleur est d'un type supérieur ou égal, l'erreur sera nulle.
\begin{figure}[h]
\centering
\includegraphics[width=0.9\columnwidth]{fig/observer_integral_nonlin/ctr.eps} 
­\caption{Suivi/rejet avec action intégrale.}
\label{fig:int:lin}
\end{figure}
Nous remarquons que la dynamique en transitoire a été modifiée par l'adjonction des deux nouveaux pôles. La commande fig. \ref{fig:com_int} semble augmenter sensiblement en raison des pôles rapides induits . 
\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{fig/observer_integral_lin/com.eps} 
­\caption{Commandes avec action intégrale.}
\label{fig:com_int}
\end{figure}

\subsection{Validation non-linéaire}
Procédons au même test sur le modèle non-linéaire de (\ref{eq:dyna}) nous remarquons, sur la fig. \ref{fig:int:nonlin}, un suivi de la référence en régime permanent comme un rejet efficace de la perturbation à l'instar du cas linéaire.
\begin{figure}[h]
\centering
\includegraphics[width=0.8\columnwidth]{fig/observer_integral_nonlin/ctr.eps} 
­\caption{Suivi/rejet avec intégrateur.}
\label{fig:int:nonlin}
\end{figure}

Notons que l'observateur n'a pas été modifié depuis sa synthèse : les erreurs de régulation ont bien été compensées grâce à l'action intégrale. Que ce soit en suivi ou en régulation, les erreurs de poursuite dues au modèle ou de régulation dues au signaux sont absorbées par la sommation de $\mbf e_y$. 

\section{Limitations des synthèses}
\label{ss:robustesse}
\subsection{Domaine de validité et linéarisation}
Premier constat, nous avons linéarisé (\ref{eq:dyna}) afin de synthétiser nos contrôleurs: tâchons d'analyser ce qu'il se passe lorsque nous dévions du point de fonctionnement nominal $\bx^*=\mbf 0$.
\begin{figure}[h]
\centering
\includegraphics[width=\columnwidth]{fig/nonlin_val.eps} 
\caption{Erreurs de poursuite angulaires à $t=15~s$ pour le modèle non-linéaire.}
\label{fig:domnonlin}
\end{figure}
Nous avons tracé l'évolution de l'erreur de poursuite à $t=15s$ angulaire pour une référence $\theta_r$ variable sur la fig. \ref{fig:domnonlin}. Nous avons choisi $\theta$ car il s'agit de la variable de sortie la plus affectée par la non-linéarité du système. Nous remarquons tout d'abord que l'imprécision des contrôleurs à \textbf{retour d'état} simple croit avec la valeur de $\theta_d$ qui s'éloigne de sa valeur nominale $\theta^*=0$. Logiquement, la dynamique avec \textbf{observateur} voit sa précision dégradée (\textit{vs} le retour d'état simple) par l'adjonction de l'erreur d'estimation. 

Pour le contrôleur avec \textbf{états augmentés}, l'intégrale finit par faire le \textit{tracking} mais cette dynamique est lente (les derniers points de la courbe demeurent des points transitoires). En contraste, le retour d'état \textbf{non-augmenté} entre en régime permanent seulement en raison des pôles qui lui a été assigné sans avoir de formulation explicite d'erreur de \textit{tracking}. Suivant notre conception, les retours d'états se stabilisent définitivement au delà de $5\mathrm{s}$.

Nous devons garder à l'esprit que la linéarisation n'est qu'une approximation valide dans un \textbf{voisinage} raisonnable, pour contrôler le pendule sur tous les segments, nous nous devons de synthétiser plusieurs contrôleurs successifs à divers $\bx^*$ et commuter entre eux en fonction du point de fonctionnement.
\subsection{Estimation des paramètres}
\begin{figure}[h]
\centering
\includegraphics[width=\columnwidth]{fig/lambdas_observer.eps} 
\caption{Évolution des pôles de $\pmb e_x$ en fonction de l'erreur $\epsilon$.}
\label{fig:evolutionlambda}
\end{figure}

Le constat précédent semble indiquer que la commande augmentée est robuste aux variations de modèle. Restons simplement dans le \textbf{cas linéaire} ici. Supposons un système avec observateur tel que l'observateur dispose d'une matrice $\mbf A' = (1+\epsilon)\mbf A$ faussée par une erreur d'estimation uniforme $\epsilon$ des paramètres $\epsilon$. 
Ceci viendra nécessairement perturber la dynamique de l'observateur, comme l'illustre la figure \ref{fig:evolutionlambda} et pourra provoquer une perte de stabilité du système, demeurant pourtant ici linéarisé. 
\begin{figure}[h]
\centering
\includegraphics[width=0.6\columnwidth]{fig/parametric15percent/ctr.eps} 
\caption{Suivi avec observateur et $\epsilon = -0.15$.}
\label{fig:suivi50p}
\end{figure}
\begin{figure}[h]
\centering
\includegraphics[width=0.6\columnwidth]{fig/parametric15percent_int/ctr.eps} 
\caption{Suivi avec observateur, action intégrale et $\epsilon = -0.15$ (modèle linéaire).}
\label{fig:suivi50pint}
\end{figure}

Nous pouvons donc constater que \textbf{la minimisation de l'erreur d'estimation} des paramètres afin de construire le système $\mbf A',\mbf B',\mbf C'$ est cruciale. Comme l'illustrent les figures \ref{fig:suivi50p} et \ref{fig:suivi50pint}, l'ajout d'un intégrateur ne présage en rien de la poursuite exacte d'un signal de référence si $\mbf A'$ est mal estimée.
\subsection{Ajout de saturations}
Le \textbf{modèle non-linéaire} (\ref{eq:dyna}) en pratique doit tenir compte des saturations des effecteurs. En effet, la force $\mbf u$ des effecteurs est bornée, nous supposerons chaque composante bornée ainsi : $|u_i|<u_{max}$. Afin de tester les influences de ces saturations nous fixons $u_{max}=10~\mathrm{N}$ : les moteurs ne peuvent que soutenir statiquement $1~\mathrm{kg}$ sous cette hypothèse. 

Nous présentons notre premier test à la fig. \ref{fig:suivisat}. Nous supposons que le chariot se trouve en présence d'une force s'opposant à son déplacement d'une valeur de $d_x = -4.5N$ (disons un frottement).  
\begin{figure}[h]
\centering
\includegraphics[width=0.6\columnwidth]{fig/integral_worst2/ctr.eps} 
\includegraphics[width=0.4\columnwidth]{fig/integral_worst2/com.eps} 
\caption{Suivi avec saturation par action intégrale et observateur.}
\label{fig:suivisat}
\end{figure}
Malgré ce frottement, qui occasionne des saturations comme nous le montre la fig \ref{fig:suivisat} la référence est \textbf{atteinte} mais la dynamique prévue par (\ref{eq:commande:integrale}) est \textbf{faussée} par cette nouvelle non-linéarité, détériorant la réponse (\textit{vs} fig. \ref{fig:int:nonlin}). Il faut donc valider nos placements de pôles et l'amplitude de nos signaux de référence en prenant une \textbf{contrainte sur l'effort de commande} pour éviter que la commande calculée par le régulateur $\bu^d$ soit $|u_i^d|>u_{max}$. 

\begin{figure}
\centering
\includegraphics[width=0.6\columnwidth]{fig/integral_worst/ctr.eps} 
\caption{Suivi avec perturbation sinusoïdale sur $u_2$ par action intégrale et observateur.}
\label{fig:suivisin}
\vspace{-2mm}
\end{figure}

Autre point, le contrôleur intégral ne permet pas le \textbf{rejet} de perturbations \textbf{non-constantes} (n'intégrant que $1/s$ dans sa structure). Supposons que $u_\theta$ soit entachée par une composante sinusoïdale (modélisation simpliste d'un écoulement tourbillonnaire si on suppose que $u_2$ est pourvue par un actionneur à hélice) $d_\theta(t) = a \mathrm{cos}(\omega t)$. 

Pour le test supposons $\omega = 10 \mathrm{rad/s}$ et $a = 2 \mathrm{N}$, les résultats de la fig. \ref{fig:suivisin} viennent confirmer nos dires : la perturbation non constante ne sera pas rejetée totalement. Toutefois, comme $a$ est modérée par rapport au dimensionnement des actionneurs $u_{max}=5a$, on remarquera que l'erreur de suivi demeure \textbf{bornée} mais oscillante à $10 \mathrm{rad/s}$ (mais \textbf{non-nulle}) gardant le profil sinusoïdal de $d_\theta$.  
\vspace{-2mm}

\section{Conclusion}
Nous pouvons conclure que l'application des synthèses précédentes doit prendre en compte \textbf{les domaines de validité} de la \emph{linéarisation} et de la \emph{commande}. Nous devons également porter attention aux sources de \textbf{perturbations non-constantes} en appliquant des méthodes de \emph{synthèse robuste} assurant que l'erreur en régulation soit \textbf{bornée} (par exemple LQR+$\mathcal{H}_{\infty}$) pour une implémentation plus adéquate et une discussion plus rigoureuse.


\end{document}
